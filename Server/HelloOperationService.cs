﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class HelloOperationService : Generated.HelloService.HelloServiceBase
    {
        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            System.Console.WriteLine(request.Name, "\n");


            return Task.FromResult(new HelloReply());
        }
    }
}
